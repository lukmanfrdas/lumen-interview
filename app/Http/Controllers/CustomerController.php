<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\File;

class CustomerController extends Controller
{
    public function index()
    {
        return Customer::paginate(3);
    }

    public function show($id)
    {
        try {
            $data = Customer::whereId($id)->first();
            $res = [
                'code' => 200,
                'message' => 'Success',
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'code' => 500,
                'message' => $e->getMessage(),
            ];
        }
        return response()->json($res, $res['code']);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'phonenumber' => 'required',
            'address' => 'required|unique:customers,phonenumber',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $input = $request->all();
            if(!empty($request->file('identity_card'))){
                $identityCard = $request->file('identity_card')->getClientOriginalName();
                $identityCard = uniqid() . '_' . $identityCard;
                $path = 'uploads/identity_card/';
                $destinationPath = $this->public_path($path); // upload path
                File::makeDirectory($destinationPath, 0777, true, true);
                $request->file('identity_card')->move($destinationPath, $identityCard);
                $input['identity_card'] = $identityCard;
            }

            $data = Customer::create($input);

            $res = [
                'code' => 200,
                'message' => 'Success',
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'code' => 500,
                'message' => $e->getMessage(),
            ];
        }
        return response()->json($res, $res['code']);

    }

    public function update(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'name'   => 'required',
        //     'phonenumber' => 'required|unique:customers,phonenumber,' . $request->id,
        //     'address' => 'required|unique:customers,phonenumber',
        // ]);
        // if ($validator->fails()) {
        //     return response()->json($validator->errors(), 422);
        // }

        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'phonenumber' => 'required',
            'address' => 'required|unique:customers,phonenumber',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $input = $request->all();
            unset($input['_method']);
            if(!empty($request->file('identity_card'))){
                $identityCard = $request->file('identity_card')->getClientOriginalName();
                $identityCard = uniqid() . '_' . $identityCard;
                $path = 'uploads/identity_card/';
                $destinationPath = $this->public_path($path); // upload path
                File::makeDirectory($destinationPath, 0777, true, true);
                $request->file('identity_card')->move($destinationPath, $identityCard);
                $input['identity_card'] = $identityCard;
            }
            Customer::whereId($input['id'])->update($input);
            $data = Customer::whereId($input['id'])->first();
            $res = [
                'code' => 200,
                'message' => 'Success',
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'code' => 500,
                'message' => $e->getMessage(),
            ];
        }
        return response()->json($res, $res['code']);
    }

    public function delete(Request $request)
    {
        try {
            $input = $request->all();
            Customer::whereId($input['id'])->delete();
            $res = [
                'code' => 200,
                'message' => 'Success',
            ];
        } catch (\Exception $e) {
            $res = [
                'code' => 500,
                'message' => $e->getMessage(),
            ];
        }
        return response()->json($res, $res['code']);
    }

    function public_path($path = null)
    {
        return rtrim(app()->basePath('public/' . $path), '/');
    }
}
